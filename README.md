# CDUtilities

[![CI Status](https://img.shields.io/travis/DinuNuta/CDUtilities.svg?style=flat)](https://travis-ci.org/DinuNuta/CDUtilities)
[![Version](https://img.shields.io/cocoapods/v/CDUtilities.svg?style=flat)](https://cocoapods.org/pods/CDUtilities)
[![License](https://img.shields.io/cocoapods/l/CDUtilities.svg?style=flat)](https://cocoapods.org/pods/CDUtilities)
[![Platform](https://img.shields.io/cocoapods/p/CDUtilities.svg?style=flat)](https://cocoapods.org/pods/CDUtilities)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CDUtilities is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CDUtilities'
```

## Author

DinuNuta, dinu.coscodan@gmail.com

## License

CDUtilities is available under the MIT license. See the LICENSE file for more info.
