//
//  DateFormater_extension.swift
//  TestEBS
//
//  Created by Dinu_c on 2/7/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation

public extension DateFormatter {
    static let iso8601Full: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()

    /// " MMMM d, YYYY "
    static let MMMMdYYYY: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM d, YYYY"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()

    static let yyyyMMdd: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()

    static let MMMddyyyy: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd yyyy"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        return formatter
    }()

    static let fullCurrentCalendar: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        formatter.calendar = Calendar.current
        return formatter
    }()

    static let iso8601WithoutTimeZone: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd' 'HH:mm:ss"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
    
    static func styled(dateStyle:DateFormatter.Style?=nil, timeStyle: DateFormatter.Style?=nil)->DateFormatter {
        let formatter = DateFormatter()
        dateStyle.flatMap({ formatter.dateStyle = $0 })
        timeStyle.flatMap({ formatter.timeStyle = $0 })
        return formatter
    }
}

public extension Date {
    var iso8601Full: String {
        DateFormatter.iso8601WithoutTimeZone.string(from: self)
    }
    var MMMMdYYYY: String {
        DateFormatter.MMMMdYYYY.string(from: self)
    }
    var MMMddyyyy: String {
        DateFormatter.MMMddyyyy.string(from: self)
    }
    var yyyyMMdd:String {
        DateFormatter.yyyyMMdd.string(from: self)
    }
    var iso8601WithoutTimeZone: String {
        DateFormatter.iso8601WithoutTimeZone.string(from: self)
    }
    var fullCurrentCalendar: String {
        DateFormatter.fullCurrentCalendar.string(from: self)
    }
    var mediumStyled:String{
        DateFormatter.styled(dateStyle: .medium).string(from: self)
    }
    var shortStyled:String{
        DateFormatter.styled(dateStyle: .short).string(from: self)
    }
    var longStyled:String{
        DateFormatter.styled(dateStyle: .long).string(from: self)
    }
}
