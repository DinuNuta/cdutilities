//
//  CDUtilities.swift
//  CDUtilities
//
//  Created by Dinu Coscodan on 5/13/20.
//

import Foundation
public protocol KeyboardApearDissappearProtocol: class {
    var scrollView: UIScrollView! {get}
    var obserHide: NSObjectProtocol? {get set}
    var observerShow: NSObjectProtocol? {get set}

    func subscribeOnKeyboardNotification()
    func removeObservers()
    func adjustForKeyboard(notification: Notification)
}

public extension KeyboardApearDissappearProtocol where Self: UIViewController {

    func subscribeOnKeyboardNotification() {
        self.obserHide = NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: OperationQueue.main, using: keyBoardWillHide(notification:))
        self.observerShow = NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: OperationQueue.main, using: keyBoardWillAppear(notification:))
    }
    func removeObservers() {
        NotificationCenter.default.removeObserver(self.observerShow ?? self)
        NotificationCenter.default.removeObserver(self.obserHide ?? self)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self)

        self.obserHide = nil
        self.observerShow = nil
    }

    func keyBoardWillAppear(notification: Notification) {
        adjustForKeyboard(notification: notification)
    }
    func keyBoardWillHide(notification: Notification) {
        adjustForKeyboard(notification: notification)
    }

    func adjustForKeyboard(notification: Notification) {
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }

        let keyboardScreenEndFrame = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)

        if notification.name == UIResponder.keyboardWillHideNotification {
            scrollView.contentInset = .zero
        } else {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        }

        self.scrollView.scrollIndicatorInsets = self.scrollView.contentInset
        self.scrollView.setNeedsLayout()
        self.view.layoutIfNeeded()
    }

}

