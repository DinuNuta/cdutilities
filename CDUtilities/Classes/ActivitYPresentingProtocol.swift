//
//  ActivitYPresentingProtocol.swift
//
//  Created by Coscodan Dinu on 3/6/18.
//  Copyright © 2018 Coscodan.Dinu. All rights reserved.
//

import Foundation
import UIKit

public protocol ActivityViewContained {
    var viewActivity: UIView! {get set}
    var activity: UIActivityIndicatorView! {get set}
    func showActivity()
    func hideActivity()
}

public extension ActivityViewContained where Self: UIViewController {
    func showActivity() {
        DispatchQueue.main.async {
            self.activity.startAnimating()
            self.viewActivity.isHidden = false
            self.viewActivity.bringSubviewToFront(self.view)
        }
    }
    func hideActivity() {
        DispatchQueue.main.async {
            self.activity.stopAnimating()
            self.viewActivity.isHidden = true
        }
    }
}

public protocol ActivitYPresentingProtocol {
    func showActivity()
    func hideActivity()
}

public extension ActivitYPresentingProtocol {
    func showActivity() {}
    func hideActivity() {}
}

//extension ActivitYPresentingProtocol
//{
//    func showActivity(){
//        DispatchQueue.main.async {
//            if let mainVC = UIApplication.topViewController() as? ActivityViewContained {
//                mainVC.showActivity()
//            }else{
//                let mainVC = UIApplication.topViewController()
//                if let cv = UIApplication.topViewController(base: mainVC?.sideMenuController?.centerViewController) as? ActivityViewContained{
//                    cv.showActivity()
//                }
//            }
//        }
//    }
//    func hideActivity(){
//        DispatchQueue.main.async {
//            if let mainVC = UIApplication.topViewController() as? ActivityViewContained {
//                mainVC.hideActivity()
//            }else{
//                let mainVC = UIApplication.topViewController()
//                if let cv = UIApplication.topViewController(controller: mainVC?.sideMenuController?.centerViewController) as? ActivityViewContained{
//                    cv.hideActivity()
//                }
//            }
//        }
//    }
//
//}
