//
//  OpeningUrlInBrowser.swift
//  TestEBS
//
//  Created by Dinu_c on 2/11/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation
import SafariServices

public var browserTintColor = UIColor.blue

public protocol OpeningURLInBrowser {
    func webOpen(_ url: URL, animated: Bool, completion: (() -> Void)?)
    func presentBrowser(safari: SFSafariViewController, animated: Bool, completion: (() -> Void)?)
}

public extension OpeningURLInBrowser {
    func webOpen(_ url: URL, animated: Bool = true, completion: (() -> Void)? = nil) {
        let config = SFSafariViewController.Configuration()
        config.entersReaderIfAvailable = false
        let safariVC = SFSafariViewController(url: url, configuration: config)
        safariVC.preferredControlTintColor = browserTintColor
        presentBrowser(safari: safariVC, animated: animated, completion: completion)
    }

    func presentBrowser(safari: SFSafariViewController, animated: Bool=true, completion: (() -> Void)? = nil) {
        if let mainVC = UIApplication.topViewController() {
            DispatchQueue.main.async {
                mainVC.present(safari, animated: animated, completion: completion)
            }
        }
    }
}

public extension OpeningURLInBrowser where Self: UIViewController {
    func presentBrowser(safari: SFSafariViewController, animated: Bool=true, completion: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            self.present(safari, animated: animated, completion: completion)
        }
    }
}
