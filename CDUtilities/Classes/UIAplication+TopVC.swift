//
//  UIAplication+TopVC.swift
//  TestEBS
//
//  Created by Dinu_c on 2/6/20.
//  Copyright © 2020 Dinu_c. All rights reserved.
//

import Foundation
import UIKit
// MARK: - Get current Top ViewController

public extension UIApplication {
    static func topViewController(base: UIViewController? = (UIApplication.shared.delegate)?.window??.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }

        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }

        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
