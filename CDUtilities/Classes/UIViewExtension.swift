//
//  UIViewExtension.swift
//
//  Created by Coscodan Dinu on 6/7/17.
//  Copyright © 2017 Coscodan.Dinu. All rights reserved.

import UIKit
public extension UIView {
    @IBInspectable var borderColor: UIColor? {
        set (newValue) {  layer.borderColor = (newValue?.cgColor ?? tintColor.cgColor) }
        get { return UIColor(cgColor: layer.borderColor!) }
    }

    @IBInspectable var borderWidth: CGFloat {
        set (newValue) { layer.borderWidth = newValue }
        get { return layer.borderWidth }
    }

    @IBInspectable var cornerRadius: CGFloat {
        set { layer.cornerRadius = newValue }
        get { return layer.cornerRadius }
    }
}

public extension UIView {
    static let shadowStyle: ShadowStyle = ShadowStyle(mask: false, color: UIColor.black, radius: 6, opacity: 0.16)

    func roundAndShadow(mask: Bool = false,
                        color: UIColor = UIColor.black,
                        shadowRadius: CGFloat = 15,
                        shadowOpacity: Float = 0.6) {
        self.layer.masksToBounds = mask
        self.layer.cornerRadius = self.bounds.height / 2
        self.layer.shadowColor = color.cgColor
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
    }

    func shadow(mask: Bool = false,
                color: UIColor = UIColor.black,
                shadowRadius: CGFloat = 15,
                shadowOpacity: Float = 0.6) {
        self.layer.masksToBounds = mask
        self.layer.shadowColor = color.cgColor
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
    }

    func setShadow(with style: ShadowStyle = shadowStyle) {
        shadow(mask: style.mask, color: style.color, shadowRadius: style.radius, shadowOpacity: style.opacity)
    }
}

public typealias ShadowStyle = (mask: Bool, color: UIColor, radius: CGFloat, opacity: Float)

public extension UIView {
    /// Helper to get pre transform frame
    var originalFrame: CGRect {
        let currentTransform = transform
        transform = .identity
        let originalFrame = frame
        transform = currentTransform
        return originalFrame
    }

    /// Helper to get point offset from center
    func centerOffset(_ point: CGPoint) -> CGPoint {
        return CGPoint(x: point.x - center.x, y: point.y - center.y)
    }

    /// Helper to get point back relative to center
    func pointRelativeToCenter(_ point: CGPoint) -> CGPoint {
        return CGPoint(x: point.x + center.x, y: point.y + center.y)
    }

    /// Helper to get point relative to transformed coords
    func newPointInView(_ point: CGPoint) -> CGPoint {
        // get offset from center
        let offset = centerOffset(point)
        // get transformed point
        let transformedPoint = offset.applying(transform)
        // make relative to center
        return pointRelativeToCenter(transformedPoint)
    }

    var newTopLeft: CGPoint {
        return newPointInView(originalFrame.origin)
    }

    var newTopRight: CGPoint {
        var point = originalFrame.origin
        point.x += originalFrame.width
        return newPointInView(point)
    }

    var newBottomLeft: CGPoint {
        var point = originalFrame.origin
        point.y += originalFrame.height
        return newPointInView(point)
    }

    var newBottomRight: CGPoint {
        var point = originalFrame.origin
        point.x += originalFrame.width
        point.y += originalFrame.height
        return newPointInView(point)
    }
}
